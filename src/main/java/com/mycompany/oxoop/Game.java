/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxoop;

/**
 *
 * @author Acer
 */import java.util.*;
public class Game {
        player PlayerX;
        player PlayerO;
        player turn;
        Table  table;
        int row,col;
        char ans;
        
        Scanner kb = new Scanner(System.in);
        public Game(){
            PlayerX =new player('X');
            PlayerO =new player('O');
            table = new Table(PlayerX,PlayerO);
            
        }
    public void ShowWelcome(){    
        System.out.println("Welcome to OX Game");
    }
    public void ShowTable(){
        table.ShowTable();
    }
    public void input(){
        while (true) {
            System.out.println("Please input Row Col :");
            row = kb.nextInt()-1;
            col = kb.nextInt()-1;
            if (table.setRowCol(row,col)){
            break;
        }
            System.out.println("Error: table at row and col is not empty!!!");
        }
    }
    public void showturn(){
        System.out.println(table.getCurren().getName()+" turn");
    }
    public void newGame(){
        System.out.println("Would you like to play the game again?. Y or N");
        ans =kb.next().charAt(0);
        if(ans == 'Y'){
            table = new Table(PlayerX,PlayerO);
        }else if(ans == 'N'){
            System.out.println("The end\n Bye Bye");
        }else{
            System.out.println("please answer Y or N");
        }
    }
    public void run(){
        this.ShowWelcome();
        while(true){
            this.ShowTable();
            this.showturn();
            this.input();
            table.checkWin();
            if(table.isFinsish()){
            if(table.getWinner() ==null){
                System.out.print("Draw!");
            }else{
                System.out.println(table.getWinner().getName()+" Win!!");
            }
            this.ShowTable();
            newGame();
        }
        table.switchplayer();
        }
    }
}
